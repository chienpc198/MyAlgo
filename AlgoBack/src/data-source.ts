import "reflect-metadata"
import { DataSource } from "typeorm"
import { BaiViet } from './entity/BaiViet'
import { Nhom } from './entity/Nhom'
import { TaiKhoan } from "./entity/TaiKhoan"
import { Code } from './entity/Code'
import { BinhLuan } from './entity/BinhLuan'

export const AppDataSource = new DataSource({
    type: "postgres",
    host: "localhost",
    port: 8060,
    username: "postgres",
    password: "190800",
    database: "myProject",
    synchronize: true,
    logging: false,
    entities: [TaiKhoan, BaiViet, Nhom, Code, BinhLuan],
    migrations: [],
    subscribers: [],
})
