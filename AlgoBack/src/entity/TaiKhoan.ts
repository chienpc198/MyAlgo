import { type } from 'os'
import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    OneToMany
} from "typeorm"
import { BaiViet } from './BaiViet'
import { BinhLuan } from './BinhLuan'
import { Code } from './Code'

@Entity()
export class TaiKhoan {

    @PrimaryGeneratedColumn()
    iMaTaiKhoan: number

    @Column()
    sTenTaiKhoan: string

    @Column()
    sMatKhau: string

    @Column({
        unique: true
    })
    sEmail: string

    @Column()
    iQuyen: number

    @OneToMany(() => BinhLuan, (binhLuan) => binhLuan.taiKhoan)
    binhLuans: BinhLuan[]

    @OneToMany(() => BaiViet, (baiViet) => baiViet.taiKhoan)
    baiViets: BaiViet[]

    @OneToMany(() => Code, (code) => code.taiKhoan)
    codes: Code[]
}
