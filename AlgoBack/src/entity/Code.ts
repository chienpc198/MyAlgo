import { 
  Entity, 
  PrimaryGeneratedColumn, 
  Column,
  ManyToOne,
  OneToOne
} from "typeorm"
import { BaiViet } from './BaiViet'
import { Nhom } from './Nhom'
import { TaiKhoan } from './TaiKhoan'

@Entity()
export class Code {

    @PrimaryGeneratedColumn()
    sMaCode: string

    @Column()
    sTenCode: string

    @Column()
    sCode: string

    @ManyToOne(() => Nhom, (nhom) => nhom.codes)
    nhom: Nhom

    @ManyToOne(() => TaiKhoan, (taiKhoan) => taiKhoan.codes)
    taiKhoan: TaiKhoan

    @OneToOne(() => BaiViet, (baiViet) => baiViet.code)
    baiViet: BaiViet
}
