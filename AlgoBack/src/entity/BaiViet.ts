import { 
  Entity, 
  PrimaryGeneratedColumn, 
  Column,
  ManyToOne,
  OneToOne,
  JoinColumn
} from "typeorm"
import { Code } from './Code'
import { Nhom } from './Nhom'
import { TaiKhoan } from './TaiKhoan'

@Entity()
export class BaiViet {

    @PrimaryGeneratedColumn()
    sMaBaiViet: string

    @Column()
    sTenBaiViet: string

    @Column()
    sText: string

    @ManyToOne(() => Nhom, (nhom) => nhom.baiViets)
    nhom: Nhom

    @ManyToOne(() => TaiKhoan, (taiKhoan) => taiKhoan.baiViets)
    taiKhoan: TaiKhoan

    @OneToOne(() => Code, (code) => code.baiViet)
    @JoinColumn()
    code: Code
}
