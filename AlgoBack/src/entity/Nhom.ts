import { 
  Entity, 
  PrimaryGeneratedColumn, 
  Column,
  OneToMany
} from "typeorm"
import { BaiViet } from './BaiViet'
import { Code } from './Code'

@Entity()
export class Nhom {

    @PrimaryGeneratedColumn()
    sMaNhom: string

    @Column()
    sTenNhom: string

    @OneToMany(() => BaiViet, (baiViet) => baiViet.nhom)
    baiViets: BaiViet[]

    @OneToMany(() => Code, (code) => code.nhom)
    codes: Code[]

}
