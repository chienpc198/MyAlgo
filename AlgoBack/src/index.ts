import { AppDataSource } from "./data-source";
import { Code } from './entity/Code';
import { Nhom } from './entity/Nhom';
import { TaiKhoan } from "./entity/TaiKhoan";
import { Request, Response } from 'express';
import { BaiViet } from './entity/BaiViet';
import { BinhLuan } from './entity/BinhLuan';

const express = require('express');
const cors = require('cors')
const bp = require('body-parser')

const app = express();

app.use(cors());

app.use(bp.json())
app.use(bp.urlencoded({ extended: true }))

AppDataSource.initialize().then(async () => {

    const taiKhoanRepository = AppDataSource.getRepository(TaiKhoan);
    const nhomRepository = AppDataSource.getRepository(Nhom);
    const codeRepository = AppDataSource.getRepository(Code);
    const baiVietRepository = AppDataSource.getRepository(BaiViet);
    const binhLuanRepository = AppDataSource.getRepository(BinhLuan);
    

    /*const admin = new TaiKhoan();
    admin.sTenTaiKhoan = 'admin';
    admin.sMatKhau = 'admin';
    admin.iQuyen = 100;
    admin.sEmail = 'chienpc198@gmail.com';

    await taiKhoanRepository.save(admin); */

    /* const nhom1 = await nhomRepository.find({
        where: {
            sMaNhom: '1'
        }
    })

    const taiKhoan = await taiKhoanRepository.find({
        where: {
            sTenTaiKhoan: 'admin'
        }
    })
    
    const code = await codeRepository.find({
        where: {
            sTenCode: 'Search'
        }
    })

    code[0].taiKhoan = taiKhoan[0];
    codeRepository.save(code[0]); */

    /* const codeTest = new Code();
    codeTest.sTenCode = 'Search';
    codeTest.sCode = 'console.log(10)'; 
    codeTest.nhom = nhom1[0]; 
    codeTest.taiKhoan = 
    await codeRepository.save(codeTest);*/

 

    /* const nhom1 = new Nhom();
    nhom1.sTenNhom = 'Sap xep';
    const nhom2 = new Nhom();
    nhom2.sTenNhom = 'Tim kiem';
    await nhomRepository.save(nhom1);
    await nhomRepository.save(nhom2); */

    /* const users = await AppDataSource.manager.find(TaiKhoan)
    console.log("Loaded users: ", users)

    console.log('Connected to database'); */
    app.get('/users/getall', async (req, res) => {
        const users = await AppDataSource.manager.find(TaiKhoan);
        res.send(users);
    });

    app.get('/nhom', async (req, res) => {
        const nhoms = await AppDataSource.manager.find(Nhom);
        res.send(nhoms);
    })

    app.post('/baiviet/thembaiviet', async (req : Request, res : Response) => {
        const baiviet = new BaiViet();
    })

    app.post('/login', async (req, res) => {
        const taiKhoan = await taiKhoanRepository.find({
            where: {
                sEmail: req.body.emailInput
            }
        })
        res.send(taiKhoan);
    })

}).catch(error => console.log(error))



app.listen(8061, () => {
    console.log('Listening on port 8061');
})