import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'
import scss from 'rollup-plugin-scss'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [svelte(), scss()]
})
