import { writable } from 'svelte/store';

export const codeTabVisibility = writable(false);