import { writable } from 'svelte/store';

export const currentUserEmail = writable('');
export const currentUserUsername = writable('');
export const currentUserQuyen = writable(0);